<?php
class Cms_m extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	var $t_post = 'cms_post';
	var $t_user = 'cms_user';
	var $t_gbook = 'cms_guest_book';
	public function all_post(){
		$query = $this->db->get($this->t_post);
		return $query->result_array();
	}
	public function all_post_search($nama=FALSE){
		$this->db->select('*');
		$this->db->like('title',$nama);
		$query	=	$this->db->get($this->t_post);
		$result	=	$query->result_array();
		return $query->result_array();
	}
	public function page_all_post_search($nama=FALSE,$p=FALSE,$limit=FALSE){
		$this->db->select('*');
		$this->db->like('title',$nama);
		$page 	= 	($p-1)*$limit;
		$query	=	$this->db->get($this->t_post,$limit, $page);
		$result	=	$query->result_array();
		return $query->result_array();
	}
	public function pag_post($p=FALSE,$limit=FALSE){
		$this->db->order_by("id", "desc");
		$page = ($p-1)*$limit;
		$query = $this->db->get($this->t_post,$limit, $page);
		return $query->result_array();
	}
	public function post($id=FALSE){
		$query = $this->db->get_where($this->t_post, array('id' => $id));
		return $query->row_array();
	}
	public function check_user($dt=FALSE){
		$query = $this->db->get_where($this->t_user,array('nama' => $dt['nama'],'pass' => md5($dt['pass'])));
		if($query->num_rows() == 1){
			return 'sukses';
		}else{
			return FALSE;
		}
	}
	public function id_post(){
		$this->db->select_max('id');
		$query = $this->db->get($this->t_post);
		$dt		= $query->row_array();
		return ($dt['id']+1);
	}
	public function insert_post($dt=FALSE){
		$data = array(
			'id' 			=> $dt['id'], 
			'title' 		=> $dt['title'], 
			'isi' 			=> $dt['isi'], 
			'url_thumb' 	=> $dt['url'], 
			'createdate' 	=> date('Y-m-d H:i:s'), 
			'author'	 	=>$dt['author']
		);
		$str = $this->db->query($this->db->insert_string($this->t_post, $data));
		if($str == 1){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	#___________GUEST BOOK_________#
	public function get_all_gBook(){
		$this->db->order_by("id", "desc");
		$query = $this->db->get($this->t_gbook);
		return $query->result_array();
	}
	
	public function get_gBook(){
		$this->db->order_by("id", "desc");
		$query = $this->db->get($this->t_gbook,5,0);
		return $query->result_array();
	}
	
	public function insert_gBook($dt=FALSE){
		$data = array(
			'id' 			=> $dt['id'], 
			'nama'	 		=> $dt['guest'], 
			'comment' 		=> $dt['comment'], 
			'createdat' 	=> $dt['tgl'] 
		);
		$str = $this->db->query($this->db->insert_string($this->t_gbook, $data));
		if($str == 1){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function edit_gBook($dt){
		$data = array(
			'id' 			=> $dt['id'], 
			'nama'	 		=> $dt['guest'], 
			'comment' 		=> $dt['comment'], 
			'createdat' 	=> $dt['date'] 
		);
		$this->db->where('id', $dt['id_p']);
		$str = $this->db->update($this->t_gbook, $data);
		if($str == 1){
			return $this->get_all_gBook();
		}else{
			return FALSE;
		}
	}
	public function search_gBook($nama=FALSE){
		$this->db->select('*');
		$this->db->like('nama',$nama);
		$this->db->or_like('comment', $nama); 
		$this->db->or_like('createdat', $nama); 
		$query	=	$this->db->get($this->t_gbook);
		$result	=	$query->result_array();
		return $query->result_array();
	}
}
