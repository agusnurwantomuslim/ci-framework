<?php
class Data_user extends CI_Model {
	public function __construct(){
		$this->load->database();
	}
	var $table = 'user';
	public function get_user($slug = FALSE){
		if($slug === FALSE){
			$query = $this->db->get('user');
			return $query->result_array();
		}else{
			$query = $this->db->get_where('user', array('nama' => $slug));
			return $query->row_array();
		}
	}
	public function insert_user($nama = FALSE){
		$data = array(
			'id'		=>	'',
			'nama' 		=> 	$nama,
			'createdat'	=>	date('Y-m-d H-i-s')
		);
		$query = $this->db->insert('user', $data); 
		return $data;
	}
	public function login_user($nama = FALSE){
		$query = $this->db->get_where('user', array('nama' => $nama));
		return $query;
	}
	public function check_user($username){
		$query = $this->db->get_where($this->table,array('nama' => $username));
		if($query->num_rows() == 1){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function get_data($nama = FALSE){
		$sql = "SELECT * FROM `user` WHERE `nama` LIKE '%$nama%'"; 
		$query = $this->db->query($sql); 
		return $query->result_array();
	}
	public function update($field,$key,$upd){
		$this->db->where($field, $key);
		$this->db->update('user', $upd); 
	}
}
