<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="description" content="intro - theme build with bootstrap">
	<meta name="author" content="Agus Nurwanto">
	<title>tabel</title>
	<link href="<?php echo base_url(); ?>perpus/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>perpus/style.css" rel="stylesheet" type="text/css">
	<script src='<?php echo base_url(); ?>perpus/js/jquery-2.0.3.min.js'></script>
	<script src='<?php echo base_url(); ?>perpus/js/custom_tabel.js'></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<center>
				<div class="center">
					<?php echo "<h2>Selamat datang $name</h2>";	#$_SESSION[username] .
						echo ""; ?>
					<form>
						<table cellspacing='0' id='all'>
							<tbody>
							<tr>
								<td colspan='5' style="border: 1px solid white;padding-bottom: 5px;">
									<a class='pointer' onclick='log_out();' style='float:left'>Log Out..</a> || 
									<a href='<?php echo base_url(); ?>index.php/cms_c/g_book'>Guest Book..</a>
									<div class="col-md-3" style='float:right; padding: 0px; height: 26px;'>
										<div class="input-group" style="height:inherit">
											<input type="text" class="form-control" style="height:inherit;padding:0 0 0 10px;" placeholder="search" id="search" onkeyup="search_enter(event)">
											<span class="input-group-btn">
												<button class="btn btn-default" type="button" style="height:inherit; padding: 1.6px 12px;" onclick='search_dt()'>
													<span class="glyphicon glyphicon-search"></span>
												</button>
											</span>
										</div><!-- /input-group -->
									 </div><!-- /.col-lg-6 -->
									<!--<input type='text' onclick='search(this.value);' id='search' value='' placeholder='search' style='float:right'/>-->
								</td>
							</tr>
							<tr style="border-top: 2px solid rgb(223, 223, 223);">
								<th style='width:30px;text-align:center;'> <input type='checkbox' name='box_all' id='box_all' onclick="checkAll(0,this.name,this.checked)"/> </th>
								<th style='width:30px'> Id </th>
								<th style='width:200px'> Post Title </th>
								<th style='width:300px'> Publish </th>
								<th style='width:100px'> Opsi </th>
							</tr>
						<?php
							#$query = mysqli_query($connect,"select * from `user`")OR die( mysqli_error($connect) );
							#while($dt = mysqli_fetch_array($query)){
							foreach($dt_user as $dt){
								echo "<tr id='tr_$dt[id]'>
										<td style='width:30px;text-align:center;'> <input type='checkbox' name='$dt[id]' id='$dt[id]'/> </td>
										<td style='width:30px;text-align:center;' onclick='my_id(\"$dt[id]\");'> 
											<span id='id$dt[id]'>$dt[id]</span> 
											<input type='text' id='id_$dt[id]' value='$dt[id]' class='col-md-12 hides' onblur='my_blur(\"$dt[id]\");' onkeypress='my_blur_id(event,\"$dt[id]\")'/>
											<input type='hidden' name='ids$dt[id]' id='ids$dt[id]' value='$dt[id]' />
										</td>
										<td style='width:200px' onclick='my_title(\"$dt[id]\");'> 
											<span id='title$dt[id]'>$dt[title]</span> 
											<input type='text' id='title_$dt[id]' value='$dt[title]' class='col-md-12 hides' onblur='my_blur(\"$dt[id]\");' onkeypress='my_blur_name(event,\"$dt[id]\")'/>
										</td>
										<td style='width:300px' onclick='date(\"$dt[id]\");'> 
											<span id='date$dt[id]'>$dt[createdate]</span> 
											<input type='text' id='date_$dt[id]' value='$dt[createdate]' class='col-md-12 hides' onblur='my_blur(\"$dt[id]\");' onkeypress='my_blur_date(event,\"$dt[id]\")'/>
										</td>
										<td style='width:100px'>
											<a href='".base_url()."index.php/cms_c/admin/$dt[id]'><span class='glyphicon glyphicon-pencil'></span></a>
											<a onclick='del($dt[id]);'><span class='glyphicon glyphicon-minus-sign'></span></a>
										</td>
									</tr>";
							}
						?>
							
							<tr>
								<td colspan='5'>
									<ul class='change'>
										<li><a onclick='del_all();'><span class='glyphicon glyphicon-remove'></span>Hapus</a></li>
										<li><a href='<?php echo base_url(); ?>index.php/cms_c/admin'><span class='glyphicon glyphicon-plus-sign'></span>Tambah data</a></li>
									</ul>
								</td>
							</tr>
							</tbody>
						</table>
					</form>
				</div>
			</center>
		</div>
	</div>
<script type='text/javascript'>
	function search_dt(){
		var title = $('#search').val();
		var numTr = $('#all tr').size();
		if(title != ''){
			$.ajax({
				url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
				type:'POST',
				data:'title='+title,
				dataType:'json',
				success:function(msg){
					for(var i = 2;i < (numTr-1); i++){
						$('#all tr:nth(2)').remove();
					}
					var msgBody = '';
					if(msg.data==undefined){
						msgBody = "<tr><td colspan='5' align='center'><b>Data tidak ditemukan!!</b></td></tr>";
					}else{
						for(var i = 0,size = msg.data.length;i<size;i++){
							msgBody = msgBody +"<tr id='tr_"+msg.data[i].id+"'>"+
											"<td style='width:30px;text-align:center;'> <input type='checkbox' name='"+msg.data[i].id+"' id='"+msg.data[i].id+"'/> </td>"+
											"<td style='width:30px;text-align:center;' onclick='my_id(\""+msg.data[i].id+"\");'> "+
												"<span id='id"+msg.data[i].id+"'>"+msg.data[i].id+"</span> "+
												"<input type='text' id='id_"+msg.data[i].id+"' value='"+msg.data[i].id+"' class='col-md-12 hides' onblur='my_blur(\""+msg.data[i].id+"\");' onkeypress='my_blur_id(event,\""+msg.data[i].id+"\")'/>"+
												"<input type='hidden' name='ids"+msg.data[i].id+"' id='ids"+msg.data[i].id+"' value='"+msg.data[i].id+"' />"+
											"</td>"+
											"<td style='width:200px' onclick='title(\""+msg.data[i].id+"\");'> "+
												"<span id='title"+msg.data[i].id+"'>"+msg.data[i].title+"</span> "+
												"<input type='text' id='title_"+msg.data[i].id+"' value='"+msg.data[i].title+"' class='col-md-12 hides' onblur='my_blur(\""+msg.data[i].id+"\");' onkeypress='my_blur_name(event,\""+msg.data[i].id+"\")'/>"+
											"</td>"+
											"<td style='width:300px' onclick='date(\""+msg.data[i].id+"\");'> "+
												"<span id='date"+msg.data[i].id+"'>"+msg.data[i].createdate+"</span> "+
												"<input type='text' id='date_"+msg.data[i].id+"' value='"+msg.data[i].createdate+"' class='col-md-12 hides' onblur='my_blur(\""+msg.data[i].id+"\");' onkeypress='my_blur_date(event,\""+msg.data[i].id+"\")'/>"+
											"</td>"+
											"<td style='width:100px'>"+
												"<a onclick='edit("+msg.data[i].id+");'><span class='glyphicon glyphicon-pencil'></span></a>"+
												"<a onclick='del("+msg.data[i].id+");'><span class='glyphicon glyphicon-minus-sign'></span></a>"+
											"</td>"+
										"</tr>";
						}
					}
					$('#all tr:nth(1)').after(msgBody);
				}
			});
		}else{
			alert('Harap di isi dulu');
		}
	}
	function search_enter(event){
		if(event.keyCode == 13){
			search_dt();
		}
	}
	function checkAll(no,title,check){
		for(var i=0;i<document.forms[no].elements.length;i++){
			var e=document.forms[no].elements[i];
			if ( (e.name!=title) && (e.type == 'checkbox') ){
				e.checked = check;
			}
		}
	}
	function log_out(){
		$.ajax({
			url:'<?php echo base_url(); ?>index.php/cms_c/logout',
			type:'POST',
			data:'nav_log_out=out',
			success:function(msg){
				window.location='<?php echo base_url(); ?>';
			}
		});
	}
	function my_blur(i){
		$('#id'+i).show();
		$('#id_'+i).hide();
		$('#title'+i).show();
		$('#title_'+i).hide();
		$('#date'+i).show();
		$('#date_'+i).hide();
	}
	function my_blur_id(event,i){
		if(event.keyCode == 13){
			my_blur(i);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
				data:'nav_id='+$('#id_'+i).val()+'&id='+$('#ids'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#id'+i).html(msg.id);
					//alert(msg.ket);
				}
			});
		}
	}
	function my_blur_name(event,i){
		if(event.keyCode == 13){
			my_blur(i);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
				data:'nav_title='+$('#title_'+i).val()+'&id='+$('#ids'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#title'+i).html(msg.title);
					//alert(msg.ket);
				}
			});
		}
	}
	function my_blur_date(event,i){
		if(event.keyCode == 13){
			my_blur(i);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
				data:'nav_date='+$('#date_'+i).val()+'&id='+$('#ids'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#date'+i).html(msg.date);
					//alert(msg.ket);
				}
			});
		}
	}
	function my_id(i){
		$('#title'+i).show();
		$('#title_'+i).hide();
		$('#date'+i).show();
		$('#date_'+i).hide();
		$('#id'+i).hide();
		$('#id_'+i).show().focus();
		
	}
	function my_title(i){
		$('#id'+i).show();
		$('#id_'+i).hide();
		$('#date'+i).show();
		$('#date_'+i).hide();
		$('#title'+i).hide();
		$('#title_'+i).show().focus();
	}
	function date(i){
		$('#id'+i).show();
		$('#id_'+i).hide();
		$('#title'+i).show();
		$('#title_'+i).hide();
		$('#date'+i).hide();
		$('#date_'+i).show().focus();
	}
	function edit(i){
		$('#id').val($('#id_'+i).val());
		$('#title').val($('#title_'+i).val());
		$('#date').val($('#date_'+i).val());
		$('#ket').val('Edit data');
		$('#id').removeAttr('disabled');
		$('#date').removeAttr('disabled');
		$('#i').val(i);
		$('#full').show();
		return false;
	}
	function del(i){
		if(confirm('Apa anda yakin untuk menghapus data dengan id '+i+' ?')==true){
			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
				data:'nav_del=nav_del&id='+$('#id_'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#all tbody #tr_'+(i)).remove();
					//alert(msg.ket);
				}
			});
		}
		return false;
	}
	function insert(){
		$('#id').attr({'disabled':'disabled'});
		$('#date').attr({'disabled':'disabled'});
		$('#ket').val('Insert data');
		$('#full').show();
		$('#title').focus();
		return false;
	}
	function remove_tr(){
		
	}
	function del_all(){ //OK
		var b = '';
		for(var i=0;i<document.forms[0].elements.length;i++){
			var e=document.forms[0].elements[i];
			if( (e.checked == true) && (e.id != 'box_all') ){
				b = b+document.getElementById('ids'+e.id).value+',';
			}
		}
		b = b.substr(0,(b.length-1));
		if(b != ''){
			if(confirm('Apa anda yakin untuk menghapus data dengan id '+b+' ?')==true){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
					data:'nav_del_all=del_all&id='+b,
					dataType:'json',
					success:function(msg){
/*
						var i = 0;
						var size = $('#all tbody').find('tr').size();
						var refreshId = setInterval(function(){
							$('#all tbody #tr_'+(msg[i])).fadeOut();
						}, 5000);
						
						clearInterval(refreshId); // stop the interval
*/
						for(var i = 0; i<msg.jml; i++){
							$('#all tbody #tr_'+(msg[i])).fadeOut();
							//setTimeOut(function(){$('#all tbody #tr_'+(msg[i])).remove()},500);
						}
						//alert(msg.ket);
					}
				});
			}
		}else{ alert('Please select checkboxs!!'); }
		return false;
	}
	/*function del_all(){
		if(confirm('Apa anda yakin untuk menghapus seluruh data yang ada?')==true){
			$.ajax({
				type:'POST',
				url:'',
				data:'nav_del_all=del_all',
				dataType:'json',
				success:function(msg){
					var size = $('#all tbody').find('tr').size();
					for(var i = 1; i<=(msg.jml-1); i++){
						$('#all tbody #tr_'+(msg.i)).remove();
					}
					//alert(msg.ket);
				}
			});
		}
		return false;
	}*/
	function close_full(){
		$('#full').hide();
		return false;
	}
	function db(){
		$('#full').hide();
		var i = $('#i').val();
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>index.php/curd',
			data:'id='+$('#id').val()+'&title='+$('#title').val()+'&date='+$('#date').val()+'&ket='+$('#ket').val()+'&i='+i,
			dataType:'json',
			success:function(msg){
				//alert(msg.ket);
				if(msg.jenis == 'edit'){
					$('#id_'+i).val(msg.id);
					$('#id'+i).html(msg.id);
					$('#title_'+i).val(msg.title);
					$('#title'+i).html(msg.title);
					$('#date_'+i).val(msg.date);
					$('#date'+i).html(msg.date);
				}else if(msg.user=='1'){
					alert('Username ini sudah ada yang pakai.. Gunakan username yang lain..');
					$('#full').show();
				}else{
					$('#all tbody tr:last-child').before(""+
	"<tr id='tr_"+(i+1)+"'>"+
		"<td style='width:30px;text-align:center;'> <input type='checkbox' name='box"+(i+1)+"' id='box"+(i+1)+"'/> </td>"+
		"<td style='width:30px;text-align:center;' onclick='my_id(\""+(i+1)+"\");'>"+
			"<span id='id"+(i+1)+"'>"+msg.id+"</span> "+
			"<input type='text' id='id_"+(i+1)+"' value='"+msg.id+"' class='col-md-12 hides' onblur='my_blur(\""+(i+1)+"\");' onkeypress='my_blur_id(event,\""+(i+1)+"\")'/>"+
			"<input type='hidden' name='ids"+(i+1)+"' id='ids"+(i+1)+"' value='"+msg.id+"' />"+
		"</td>"+
		"<td style='width:200px' onclick='title(\""+(i+1)+"\");'> "+
			"<span id='title"+(i+1)+"'>"+msg.title+"</span> "+
			"<input type='text' id='title_"+(i+1)+"' value='"+msg.title+"' class='col-md-12 hides' onblur='my_blur(\""+(i+1)+"\");' onkeypress='my_blur_name(event,\""+(i+1)+"\")'/>"+
		"</td>"+
		"<td style='width:300px' onclick='date(\""+(i+1)+"\");'> "+
			"<span id='date"+(i+1)+"'>"+msg.date+"</span> "+
			"<input type='text' id='date_"+(i+1)+"' value='"+msg.date+"' class='col-md-12 hides' onblur='my_blur(\""+(i+1)+"\");' onkeypress='my_blur_date(event,\""+(i+1)+"\")'/>"+
		"</td>"+
		"<td style='width:100px'>"+
			"<a onclick='edit("+(i+1)+");'><span class='glyphicon glyphicon-pencil'></span></a>"+
			"<a onclick='del("+(i+1)+");'><span class='glyphicon glyphicon-minus-sign'></span></a>"+
		"</td>"+
	"</tr>");
				}
			}
		});
		return false;
	}
</script>
</body>
</html>
