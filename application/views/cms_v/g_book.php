<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="description" content="intro - theme build with bootstrap">
	<meta name="author" content="Agus Nurwanto">
	<title>Guest book</title>
	<link href="<?php echo base_url(); ?>perpus/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>perpus/style.css" rel="stylesheet" type="text/css">
	<script src='<?php echo base_url(); ?>perpus/js/jquery-2.0.3.min.js'></script>
	<script src='<?php echo base_url(); ?>perpus/js/custom_tabel.js'></script>
</head>
<body>
	<div id='full'>
		<div id='overlay'>
			<form name='form[0]' action='<?php echo base_url();?>index.php/cms_c/g_book' method='POST'>
				<div class='head'>
					<input type='text' id='ket' value='' />
					<a onclick="close_full();"><span class='glyphicon glyphicon-remove-circle'></span></a>
				</div>
				<ul class='data' style='display:block'>
					<li style='display:none'>
						<label for='id'>Id</label>
						<input type='text' id='ket2' name='ket' value='' />
						<input type='text' name='id' id='id' value='' placeholder='Id User'/>
						<input type='hidden' name='id_primer' id='id_primer' value='' placeholder='Id User'/>
					</li>
					<li>
						<label for='nama'>Nama</label>
						<input type='text' name='guest' id='nama' value='' placeholder='User Name'/>
					</li>
					<li>
						<label for='nama'>Komentar</label>
						<textarea name='comment' id='comment' placeholder='Komentar'></textarea>
					</li>
				<?php if(!empty($nama)){ ?>
					<li>
						<input type='text' name='date' id='date' value='' />
					</li>
				<?php } ?>
					<li>
						<input type='submit' name='submit' id='submit' value='Submit' />
					</li>
				</ul>
			</form>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<center>
				<div class="center">
					<?php echo "<h2>Selamat Datang</h2>"; ?>
					<form>
						<a href='<?php echo base_url(); ?>'>Home..</a> 
						<?php if(!empty($nama)){ ?>
						 || <a href='<?php echo base_url(); ?>index.php/cms_c/logout'>Log out..</a> 
						 || <a href='<?php echo base_url(); ?>index.php/cms_c/admin'>Admin..</a> 
						 <?php }else{ ?>
						 || <a href='<?php echo base_url(); ?>index.php/cms_c/login'>Sign in..</a>
						 <?php } ?>
						<table cellspacing='0' id='all'>
							<tbody>
							<tr>
								<td colspan='6' style="border: 1px solid white;padding-bottom: 5px;">
									<div class="col-md-3" style='float:right; padding: 0px; height: 26px;'>
										<div class="input-group" style="height:inherit">
											<input type="text" class="form-control" style="height:inherit;padding:0 0 0 10px;" placeholder="search" id="search" onkeyup="search_enter(event);return false;">
											<span class="input-group-btn" style='height:inherit'>
												<button class="btn btn-default" style="height:inherit; padding: 1.6px 12px;" onclick='search_dt();return false;'>
													<span class="glyphicon glyphicon-search"></span>
												</button>
											</span>
										</div><!-- /input-group -->
									 </div><!-- /.col-lg-6 -->
									<!--<input type='text' onclick='search(this.value);' id='search' value='' placeholder='search' style='float:right'/>-->
								</td>
							</tr>
							<tr style="border-top: 2px solid rgb(223, 223, 223);">
							<?php if(!empty($nama)){ ?>
								<th style='width:30px;text-align:center;'> <input type='checkbox' name='box_all' id='box_all' onclick="checkAll(1,this.name,this.checked)"/> </th>
							<?php } ?>	
								<th style='width:30px'> No </th>
								<th style='width:200px'> User Name </th>
								<th style='width:300px'> Comment </th>
								<th style='width:300px'> Publish </th>
							<?php if(!empty($nama)){ ?>
								<th style='width:100px'> Opsi </th>
							<?php } ?>
							</tr>
						<?php
							$no = 0;
							foreach($dt_guest as $dt){
								$no++;
								echo "<tr id='tr_$dt[id]'>";
								if(!empty($nama)){
										echo "<td style='width:30px;text-align:center;'> <input type='checkbox' name='$dt[id]' id='$dt[id]'/> </td>";
								}
								echo "<td style='width:30px;text-align:center;'> 
											<span id='id_$dt[id]'>$no</span> 
											<input type='hidden' value='$dt[id]' id='id_primer_$dt[id]' /> 
										</td>
										<td style='width:200px'> 
											<span id='nama_$dt[id]'>$dt[nama]</span> 
										</td>
										<td style='width:200px'> 
											<span id='comment_$dt[id]'>$dt[comment]</span> 
										</td>
										<td style='width:300px'> 
											<span id='date_$dt[id]'>$dt[createdat]</span> 
										</td>";
										
								if(!empty($nama)){
									echo "<td style='width:100px'>
											<a onclick='edit($dt[id]);'>
												<span class='glyphicon glyphicon-pencil'></span>
											</a>
											<a onclick='del($dt[id]);'>
												<span class='glyphicon glyphicon-minus-sign'></span>
											</a>
										</td>
									</tr>";
								}
							}
						?>
							
							<tr>
								<td colspan='6'>
									<ul class='change'>
									<?php if(!empty($nama)){ ?>
										<li><a onclick='del_all();'><span class='glyphicon glyphicon-remove'></span>Hapus</a></li>
									<?php } ?>
										<li><a onclick='tambah();'><span class='glyphicon glyphicon-plus-sign'></span>Tambah data</a></li>
									</ul>
								</td>
							</tr>
							</tbody>
						</table>
					</form>
				</div>
			</center>
		</div>
	</div>
<script type='text/javascript'>
	function tambah(){
		$('#ket').attr({'disabled':'disabled'});
		$('#ket').val('Insert data');
		$('#ket2').val('Insert data');
		$('#full').show();
		$('#nama').focus();
		return false;
	}
	function search_dt(){
		var v = $('#search').val();
		window.location='<?php echo base_url(); ?>index.php/cms_c/g_book/'+v;
	}
	function search_enter(event){
		if(event.keyCode == 13){
			search_dt();
		}
	}
	
	function checkAll(no,nama,check){
		for(var i=0;i<document.forms[no].elements.length;i++){
			var e=document.forms[no].elements[i];
			if ( (e.name!=nama) && (e.type == 'checkbox') ){
				e.checked = check;
			}
		}
	}
	function log_out(){
		$.ajax({
			url:'<?php echo base_url(); ?>index.php/cms_c/logout',
			type:'POST',
			data:'nav_log_out=out',
			success:function(msg){
				window.location='<?php echo base_url(); ?>';
			}
		});
	}
	function edit(i){
		$('#id').val($('#id_primer_'+i).val());
		$('#nama').val($('#nama_'+i).text());
		$('#comment').val($('#comment_'+i).text());
		$('#date').val($('#date_'+i).text());
		$('#id_primer').val($('#id_primer_'+i).val());
		$('#ket').attr({'disabled':'disabled'});
		$('#ket').val('Edit data');
		$('#ket2').val('Edit data');
		$('#i').val(i);
		$('#full').show();
		return false;
	}
	function del(i){
		var nama = $('#nama_'+i).text();
		if(confirm('Apa anda yakin untuk menghapus comment dari '+nama+' ?')==true){
			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
				data:'nav_del=nav_del&id='+$('#id_'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#all tbody #tr_'+(i)).remove();
					//alert(msg.ket);
				}
			});
		}
		return false;
	}
	
	function remove_tr(){
		
	}
	function del_all(){ //OK
		var b = '';
		for(var i=0;i<document.forms[0].elements.length;i++){
			var e=document.forms[0].elements[i];
			if( (e.checked == true) && (e.id != 'box_all') ){
				b = b+document.getElementById('ids'+e.id).value+',';
			}
		}
		b = b.substr(0,(b.length-1));
		if(b != ''){
			if(confirm('Apa anda yakin untuk menghapus data dengan id '+b+' ?')==true){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url(); ?>index.php/cms_c/all_admin',
					data:'nav_del_all=del_all&id='+b,
					dataType:'json',
					success:function(msg){
/*
						var i = 0;
						var size = $('#all tbody').find('tr').size();
						var refreshId = setInterval(function(){
							$('#all tbody #tr_'+(msg[i])).fadeOut();
						}, 5000);
						
						clearInterval(refreshId); // stop the interval
*/
						for(var i = 0; i<msg.jml; i++){
							$('#all tbody #tr_'+(msg[i])).fadeOut();
							//setTimeOut(function(){$('#all tbody #tr_'+(msg[i])).remove()},500);
						}
						//alert(msg.ket);
					}
				});
			}
		}else{ alert('Please select checkboxs!!'); }
		return false;
	}
	function close_full(){
		$('#full').hide();
		return false;
	}
</script>
</body>
</html>
