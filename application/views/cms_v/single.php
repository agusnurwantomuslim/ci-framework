<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
</head>
<body>
	<a href='<?php echo base_url(); ?>index.php/cms_c/login'>Sign in..</a><br>
	<a href="<?php echo base_url(); ?>">Home..</a><br>
	<br>
<?php
	$message = $this->session->flashdata('message');
	echo $message == '' ? '': '<p>'.$message.'</p>';
?>
	<h2><?php echo $title; ?></h2>
	<img src="<?php echo $url_thumb; ?>" width="200px">
	<p><?php echo $isi; ?></p>
	<p><?php echo $createdate; ?></p>
	<p><?php echo $author; ?></p>
</body>
</html>
