<?php
class Home extends CI_Controller {
	public function __construct(){		#Untuk exsekusi fungsi ketika pertamakali class di load
		parent::__construct();			#Menggunakan construct parrent milik CI_Controller
		#parent::Controller();			#Menggunakan construct parrent milik CI_Controller
		#$this->load->database(); 		#Untuk conection DB manual
		#$this->load->model('data_user','',TRUE);	#Jalankan file data_user.php		#penggunaan true untuk koneksi DB
		$this->load->model('data_user');	#Tidak perlu memakai true karena DB sudah terkoneksi otomatis karena di daftarkan di autoload.php 'libraries'
	}
	public function index(){
		if($this->session->userdata('username') == TRUE){
			redirect('curd');
		}else{
			$data['dt_user'] = $this->data_user->get_user();
			$data['title'] = 'Member List';
			$this->load->view('user/index', $data);
		}
	}
	public function login(){
		$this->form_validation->set_rules('nama','Username','required');
		#$this->form_validation->set_rules('password','Password','required');
		$data['dt_user'] = $this->data_user->get_user();
		$data['title'] = 'Member List';
		if($this->form_validation->run() == TRUE){
			$username = $this->input->post('nama');
			#$password = $this->input->post('password');
			#if($this->Login_model->check_user($username,$password) == TRUE){
			if($this->data_user->check_user($username) == TRUE){
				$data = array('username' => $username, 'ket' => 'Sukses!');
				$this->session->set_userdata($data);
				$msg['href'] = 'Y';
				#$msg['ket'] = '<h2>Welcome!!</h2>';# masuk
				die(json_encode($msg));
				#redirect('curd');
			}else{
				$this->session->set_flashdata('message','Maaf, username Anda salah');
				$msg['ket'] = '<h2>You not a member!!</h2>';# tidak masuk
				$msg['href'] = 'N';
				die(json_encode($msg));
				#redirect('home');
			}
		}else{
			$msg['ket'] = '<h2>Please insert your username!!</h2>';
			$msg['href'] = 'N';
			die(json_encode($msg));
			#$this->load->view('user/index', $data);
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		#redirect('home','refresh');
	}
		/*$nama = $_POST['nama'];
		$data['dt_user'] = '';
		if(!empty($nama)){
			$data['dt_user'] = $this->data_user->login_user($nama);
			if($data['dt_user']==1){''}
		}
		die(json_decode($data));
	}*/
}
