<?php
class Cms_c extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('cms_m');
	}
	var $t_post = 'cms_post';
	var $t_user = 'cms_user';
	public function index($page=1){
		$this->load->library('pagination');
		$dt_all = $this->cms_m->all_post();
		$config['base_url'] = $this->config->config['base_url'].'index.php/cms_c/index/';
		$config['total_rows'] = count($dt_all);
		$config['per_page'] = 2;
		$config['uri_segment'] = 3;
		$config['num_links'] = 2;
		$config['use_page_numbers'] = TRUE;
		#$config['page_query_string'] = TRUE;
		$dt_db['db'] = $this->cms_m->pag_post($page,$config['per_page']);
		$dt_db['gb'] = $this->cms_m->get_gBook();
		if($this->session->userdata('username') == TRUE){
			$dt_db['in'] = 'Y';
		}
		$this->pagination->initialize($config);
		$dt_db['pagination'] = $this->pagination->create_links();
		$this->load->view('cms_v/index',$dt_db);
	}
	public function single($id_post = FALSE){
		$post_db = $this->cms_m->post($id_post);
		$this->load->view('cms_v/single',$post_db);
	}
	public function search_index($page=1){
		$this->load->library('pagination');
		$nama = $this->input->post('search');
		if(!empty($nama)){
			$nama = $nama;
			$this->session->set_userdata(array('search'=>$nama));
		}
		$dt_all = $this->cms_m->all_post_search($nama);
		$config['base_url'] = $this->config->config['base_url'].'index.php/cms_c/search_index';
		$config['total_rows'] = count($dt_all);
		$config['per_page'] = 2;
		$config['uri_segment'] = 3;
		$config['num_links'] = 2;
		$config['use_page_numbers'] = TRUE;
		#$config['page_query_string'] = TRUE;
		$dt_db['db'] = $this->cms_m->page_all_post_search($nama,$page,$config['per_page']);
		$this->pagination->initialize($config);
		$dt_db['pagination'] = $this->pagination->create_links();
		$dt_db['search'] = $nama;
		$dt_db['gb'] = $this->cms_m->get_gBook();
		$dt_db['searchs'] = 'Y';
		if($this->session->userdata('username') == TRUE){
			$dt_db['in'] = 'Y';
		}
		$this->load->view('cms_v/index',$dt_db);
	}
	public function login(){
		if($this->session->userdata('username') == TRUE){
			redirect('cms_c/admin');
		}else{
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Data harus diisi!!');
			$this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
			$config = array(
				   array(
						 'field'   => 'nama',
						 'label'   => 'Nama',
						 'rules'   => 'required'
					  ),
				   array(
						 'field'   => 'password',
						 'label'   => 'Password',
						 'rules'   => 'required'
					  )
				);
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == TRUE){
				$dt['nama'] = $this->input->post('nama');
				$dt['pass'] = $this->input->post('password');
				if($this->cms_m->check_user($dt)==TRUE){
					$this->session->set_userdata(array('username' => $dt['nama']));
					redirect('cms_c/admin');
				}else{
					$ket['ket'] = '<div style="color:red">Data yang Anda masukan salah.. Harap cek kembali</div>';
					$this->load->view('cms_v/form',$ket);
				}
			}else{
				$ket['ket']='';
				$this->load->view('cms_v/form',$ket);
			}
		}
	}
	public function admin($edit=FALSE){
		$dt_user = $this->session->userdata('username');
		if(!empty($dt_user)){
			echo "<h3>Selamat datang $dt_user</h3>";
		}
		if($this->session->userdata('username') == TRUE){
			$config['upload_path'] = './uploads';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '100';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

			$this->load->library('upload', $config);
			
			$this->load->library('form_validation');
			$this->load->helper(array('form', 'url'));
			$this->form_validation->set_message('required', 'Data harus diisi!!');
			$this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
			$this->session->set_flashdata('message','<h2 style="color:red">Coba Lagi, post anda tidak tersimpan</h2>');
			$status = '<h2 style="color:red">Gagal tersimpan</h2>';
			$config = array(
				   array(
						 'field'   => 'title',
						 'label'   => 'Title',
						 'rules'   => 'required'
					  ),
				   array(
						 'field'   => 'isi',
						 'label'   => 'Isi',
						 'rules'   => 'required'
					  )
				);
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == TRUE){
				if ( ! $this->upload->do_upload('image')){
					$dt['id']		= $this->cms_m->id_post();
					$dt['title'] 	= $this->input->post('title');
					$dt['isi'] 		= $this->input->post('isi');
					$dt['url'] 		= '';
					$dt['author'] 	= $this->session->userdata('username');
					$status 		= $this->cms_m->insert_post($dt);
					if($status == TRUE){
						$this->session->set_flashdata('message','<h2 style="color:green">Tersimpan</h2>');
						$post_db = $this->cms_m->post($dt['id']);
						redirect('cms_c/single/'.$dt['id'],$post_db);
					}
					$ket = array(
						'ket' 		=> '<div style="color:red">'.$this->upload->display_errors().'</div>',
						'status'	=> $status,
						'id'		=> $dt['id'],
						'title'		=> '',
						'isi'		=> '',
						'display'	=> 'display:none',
						'show'		=> '<a href="'.base_url().'index.php/cms_c/single/'.$dt['id'].'">Show Post..</a>'
					);
					
					$this->load->view('cms_v/admin', $ket);
				}else{
					if($this->upload->data()==TRUE){
						$dt_img 			= $this->upload->data();
					}else{
						$dt_img['file_name']= '';
					}
					$dt['id']			= $this->cms_m->id_post();
					$dt['title'] 		= $this->input->post('title');
					$dt['isi'] 			= $this->input->post('isi');
					$dt['url'] 			= $this->config->config['base_url'].'uploads/'.$dt_img['file_name'];
					$dt['author'] 		= $this->session->userdata('username');
					$status = $this->cms_m->insert_post($dt);
					if($status == TRUE){
						$this->session->set_flashdata('message','<h2 style="color:green">Tersimpan</h2>');
						redirect('cms_c/single/'.$dt['id'],$post_db);
					}
					$ket = array(
						#'ket' 		=> $this->upload->data(),
						'title'		=> '',
						'isi'		=> '',
						'display'	=> '',
						'id'		=> $dt['id'],
						'ket'		=> '<div style="color:green">Sukses</div>',
						'status'	=> $status,
						'show'		=> '<a href="'.base_url().'index.php/cms_c/single/'.$dt['id'].'">Show Post..</a>'
					);
					$this->load->view('cms_v/admin', $ket);
				}
			}else{
				$ket = array(
						'id'		=> '',
						'title'		=> '',
						'isi'		=> '',
						'display'	=> 'display:none',
						'ket'		=> '',
						'status'	=> '',
						'show'		=> ''
					);
				if($edit!=FALSE){
					$dt = $this->cms_m->post($edit);
					$ket = array(
						#'ket' 		=> $this->upload->data(),
						'title'		=> $dt['title'],
						'isi'		=> $dt['isi'],
						'display'	=> '',
						'id'		=> $dt['id'],
						'ket'		=> '<br><img src="'.$dt['url_thumb'].'" width="200"><br>',
						'status'	=> '',
						'show'		=> '<a href="'.base_url().'index.php/cms_c/single/'.$dt['id'].'">Show Post..</a>'
					);
				}
				$this->load->view('cms_v/admin', $ket);
				
			}
		}else{
			redirect('cms_c');
		}
	}
	public function all_admin(){
		$data['name'] = $this->session->userdata('username');
		$data['dt_user'] = $this->cms_m->all_post();
		$this->load->view('cms_v/all_admin', $data);
		$id_baru 		= $this->input->post('nav_id');
		$nama_baru 		= $this->input->post('nav_title');
		$date_baru 		= $this->input->post('nav_date');
		$nav_del 		= $this->input->post('nav_del');
		$nav_del_all 	= $this->input->post('nav_del_all');
		if(!empty($id_baru)){
			$id = $this->input->post('id');
			$upd = array('id'=>$id_baru);
			$this->data_user->update('id',$id,$upd);
			$ket['id']=$id_baru;
			die(json_encode($ket));
		}else if(!empty($nama_baru)){
			$id = $this->input->post('id');
			$upd = array('title'=>$nama_baru);
			$this->data_user->update('id',$id,$upd);
			$ket['title']=$nama_baru;
			die(json_encode($ket));
		}else if(!empty($date_baru)){
			$id = $this->input->post('id');
			$upd = array('createdate'=>$date_baru);
			$this->data_user->update('id',$id,$upd);
			$ket['date']=$date_baru;
			die(json_encode($ket));
		}else if($nav_del=='nav_del'){
			$id 		= $this->input->post('id');
			$ket['ket']	='gagal';
			if($this->db->delete($this->t_post, array('id' => $id))==true){
				$ket['ket']='sukses';
			}
			die(json_encode($ket));
		}else if(!empty($_POST['ket'])){
			$i 		= $this->input->post('i');
			$id 	= $this->input->post('id');
			$nama 	= $this->input->post('title');
			$date 	= $this->input->post('date');
			$ket['id']		=	$id;
			if( !empty($id) && !empty($nama) && !empty($date) && ($_POST['ket']=='Edit data') ){
				$data = array(
					'id'	=>	$id,
					'title'	=>	$nama,
					'createdate'	=>	$date
				);
				$this->data_user->update('id',$id,$data);
				$ket['jenis']	= 'edit';
			}else if($_POST['ket']=='Insert data'){
				$this->db->where('title', $nama);
				$this->db->from('user');
				$username 	= 	$this->db->count_all_results();
				$ket['user']= 1;
				if($username==0){
					$date 	= date('Y-m-d H:i:s');
					$this->db->flush_cache();
					$this->db->select_max('id');
					$query = $this->db->get('user');
					$dt		= $query->row_array();
					$data 	= array(
						'id'	=>	($dt['id']+1),
						'title'	=>	$nama,
						'createdate'	=>	$date
					);
					$this->db->insert('user', $data);
					$ket['id']	=	($dt['id']+1);
					$ket['jenis']= 'insert';
					$ket['user']= 0;
				}
				$ket['jumlah_name'] = $username;
			}
			$ket['title']	=	$nama;
			$ket['createdate']	=	$date;
			!empty($i)? $ket['i']=$i : $ket['i']='';
			die(json_encode($ket));
		}else if($nav_del_all=='del_all'){
			$dt_id 		= $this->input->post('id');
			$id = explode(',',$dt_id);
			for( $i=0,$size=count($id);$i<$size;$i++ ){
				$this->db->delete($this->t_post, array('id' => $id[$i]));
				$ket[$i] = $id[$i];
			}
			$ket['ket']='sukses';
			$ket['jml'] = $size;
			die(json_encode($ket));
		}
	}
	public function search(){
		$nama = $this->input->post('nama');
		$data_db = $this->data_user->get_data($nama);
		$data['ket']='Y';
		if(empty($data_db)){
			$data['ket'] = 'Data dengan kata kunci "'.$nama.'" tidak ada, coba dengan kata kunci lain';
		}else{
			$data['data'] = $data_db;
		}
		die(json_encode($data));
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('cms_c');
	}
	public function g_book($search=FALSE){
		$ket	= $this->input->post('ket');
		$delete = $this->input->post('delete');
		$callback['nama'] = $this->session->userdata('username');
		if($ket == 'Insert data'){
			$dt['id'] = $this->input->post('id');
			$dt['guest'] = $this->input->post('guest');
			$dt['comment'] = $this->input->post('comment');
			$dt['tgl'] = date('Y-m-d H-i-s');
			$callback['chek'] = $this->cms_m->insert_gBook($dt);
			$callback['dt_guest'] = $this->cms_m->get_all_gBook();
			$this->load->view('cms_v/g_book',$callback);
		}else if($ket=='Edit data'){
			$dt['id_p'] = $this->input->post('id_primer');
			$dt['id'] = $this->input->post('id');
			$dt['guest'] = $this->input->post('guest');
			$dt['comment'] = $this->input->post('comment');
			$dt['date'] = $this->input->post('date');
			$callback['dt_guest'] = $this->cms_m->edit_gBook($dt);
			$this->load->view('cms_v/g_book',$callback);
		}else if(!empty($delete)){
			$dt['id'] = $this->input->post('id');
			$callback = $this->cms_m->del_gBook();
			$this->load->view('cms_v/g_book',$callback);
		}else if(!empty($search)){
			$callback['dt_guest'] = $this->cms_m->search_gBook($search);
			$this->load->view('cms_v/g_book',$callback);
		}else{
			$callback['dt_guest'] = $this->cms_m->get_all_gBook();
			$this->load->view('cms_v/g_book',$callback);
		}
	}
}
