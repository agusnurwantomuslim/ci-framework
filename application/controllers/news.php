<?php
class News extends CI_Controller {

	public function __construct()		#Untuk exsekusi fungsi ketika pertamakali class di load
	{
		parent::__construct();			#Menggunakan construct parrent milik CI_Controller
		$this->load->model('news_model');	#Jalankan file news_model.php
	}

	/*public function index()				#Mendapatkan data news ketika url = domain.com/index.php/news 		#dari folder model
	{
		echo "index";
		#$data['news'] = $this->news_model->index();
		$data['news'] = $this->news_model->index();
		echo "<pre>".print_r( $data['news'],1 )."</pre>";
	}*/

	
	public function index()
	{
		echo "<br>index news_models.php";
		$data['news'] = $this->news_model->get_news();
		$data['title'] = 'News archive';
		echo "<pre>".print_r( $data,1 )."</pre>";
		
		$this->load->view('templates/header', $data);
		$this->load->view('news/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug)			#Mendapatkan data news ketika url = domain.com/index.php/news/view 		#dari forder model dengan fungsi get_news
	{
		#echo "view()";
		$data['news_item'] = $this->news_model->get_news($slug);
		echo "<pre>".print_r( $data,1 )."</pre>";
		
		#$data['news_item'] = $this->news_model->index($slug);		#Karena ada 2 fungsi jadi bisa pake index atau get_news

		if (empty($data['news_item']))
		{
			show_404();
		}

		$data['title'] = $data['news_item']['title'];

		$this->load->view('templates/header', $data);
		$this->load->view('news/view', $data);
		$this->load->view('templates/footer');
	}
}
