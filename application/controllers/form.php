<?php
class Form extends CI_Controller {
	public function __construct(){		#Untuk exsekusi fungsi ketika pertamakali class di load
		parent::__construct();			#Menggunakan construct parrent milik CI_Controller
	}
	public function index(){
		$this->load->library('form_validation');
		$this->form_validation->set_message('required', 'Data harus diisi!!');
		$this->form_validation->set_message('alpha_dash_space', 'Nama tidak boleh angka!!');
		$this->form_validation->set_message('valid_email', 'Email anda salah!!');
		$this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
		$config = array(
               array(
                     'field'   => 'nama',
                     'label'   => 'Nama',
                     'rules'   => 'required|trim|xss_clean|callback_alpha_dash_space'
                  ),
               array(
                     'field'   => 'email',
                     'label'   => 'Email',
                     'rules'   => 'valid_email'
                  ),
               array(
                     'field'   => 'umur',
                     'label'   => 'Umur',
                     'rules'   => 'required|numeric|greater_than[15]|less_than[100]'
                  )
            );
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == TRUE){
			redirect('cms_v/formsuces');
		}else{
			$this->load->view('cms_v/form');
		}
	}
	public function alpha_dash_space($str){
		return (! preg_match("/^([A-Za-z_ ])+$/i", $str)) ? FALSE : TRUE;
	}
}
