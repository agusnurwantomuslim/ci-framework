<?php
class Curd extends CI_Controller {		#Pertanyaanya kenapa class 
	public function __construct(){		#Untuk exsekusi fungsi ketika pertamakali class di load
		parent::__construct();			#Menggunakan construct parrent milik CI_Controller
		$this->load->model('data_user');		#Jika menggunakan fungsi dari dir.model maka class harus diload
	}
	public function index(){
		if($this->session->userdata('username') == ''){
			redirect('home');
		}else{
			$id_baru 		= $this->input->post('nav_id');
			$nama_baru 		= $this->input->post('nav_nama');
			$date_baru 		= $this->input->post('nav_date');
			$nav_del 		= $this->input->post('nav_del');
			$nav_del_all 	= $this->input->post('nav_del_all');
			if(!empty($id_baru)){
				$id = $this->input->post('id');
				$upd = array('id'=>$id_baru);
				$this->data_user->update('id',$id,$upd);
				$ket['id']=$id_baru;
				die(json_encode($ket));
			}else if(!empty($nama_baru)){
				$id = $this->input->post('id');
				$upd = array('nama'=>$nama_baru);
				$this->data_user->update('id',$id,$upd);
				$ket['nama']=$nama_baru;
				die(json_encode($ket));
			}else if(!empty($date_baru)){
				$id = $this->input->post('id');
				$upd = array('createdat'=>$date_baru);
				$this->data_user->update('id',$id,$upd);
				$ket['date']=$date_baru;
				die(json_encode($ket));
			}else if($nav_del=='nav_del'){
				$id 		= $this->input->post('id');
				$ket['ket']	='gagal';
				if($this->db->delete('user', array('id' => $id))==true){
					$ket['ket']='sukses';
				}
				die(json_encode($ket));
			}else if(!empty($_POST['ket'])){
				$i 		= $this->input->post('i');
				$id 	= $this->input->post('id');
				$nama 	= $this->input->post('nama');
				$date 	= $this->input->post('date');
				$ket['id']		=	$id;
				if( !empty($id) && !empty($nama) && !empty($date) && ($_POST['ket']=='Edit data') ){
					$data = array(
						'id'	=>	$id,
						'nama'	=>	$nama,
						'createdat'	=>	$date
					);
					$this->data_user->update('id',$id,$data);
					$ket['jenis']	= 'edit';
				}else if($_POST['ket']=='Insert data'){
					$this->db->where('nama', $nama);
					$this->db->from('user');
					$username 	= 	$this->db->count_all_results();
					$ket['user']= 1;
					if($username==0){
						$date 	= date('Y-m-d H:i:s');
						$this->db->flush_cache();
						$this->db->select_max('id');
						$query = $this->db->get('user');
						$dt		= $query->row_array();
						$data 	= array(
							'id'	=>	($dt['id']+1),
							'nama'	=>	$nama,
							'createdat'	=>	$date
						);
						$this->db->insert('user', $data);
						$ket['id']	=	($dt['id']+1);
						$ket['jenis']= 'insert';
						$ket['user']= 0;
					}
					$ket['jumlah_name'] = $username;
				}
				$ket['nama']	=	$nama;
				$ket['date']	=	$date;
				!empty($i)? $ket['i']=$i : $ket['i']='';
				die(json_encode($ket));
			}else if($nav_del_all=='del_all'){
				$id_p = $this->input->post('id');
				$id = explode(',',$id_p);
				$ket['ket']='sukses';
				for($i = 0,$size=count($id);$i<$size;$i++){
					$this->db->delete('user', array('id' => $id[$i]));
				}
				die(json_encode($ket));
			}
			
			$data['name'] = $this->session->userdata('username');
			$data['dt_user'] = $this->data_user->get_user();
			$this->load->view('user/curd', $data);
		}
	}
	public function search(){
		$nama = $this->input->post('nama');
		$data_db = $this->data_user->get_data($nama);
		$data['ket']='Y';
		if(empty($data_db)){
			$data['ket'] = 'Data dengan kata kunci "'.$nama.'" tidak ada, coba dengan kata kunci lain';
		}else{
			$data['data'] = $data_db;
		}
		die(json_encode($data));
	}
}
